# [1.3.0](https://gitlab.com/depixy/image/compare/v1.2.1...v1.3.0) (2020-07-23)


### Bug Fixes

* remove uncessary type ([429ce2c](https://gitlab.com/depixy/image/commit/429ce2c7cb32715995beba234b491223edd68d07))


### Features

* add delete ([b62fde0](https://gitlab.com/depixy/image/commit/b62fde006597857175256f8f6939efa63311ff37))

## [1.2.1](https://gitlab.com/depixy/image/compare/v1.2.0...v1.2.1) (2020-07-23)


### Bug Fixes

* update middlewares ([7f9b259](https://gitlab.com/depixy/image/commit/7f9b2599ab54bc919d944443e5aca105afecd471))

# [1.2.0](https://gitlab.com/depixy/image/compare/v1.1.1...v1.2.0) (2020-07-23)


### Features

* add format options ([c97dd13](https://gitlab.com/depixy/image/commit/c97dd1366c63ab686d6beeddb7b89a734d0cf509))

## [1.1.1](https://gitlab.com/depixy/image/compare/v1.1.0...v1.1.1) (2020-07-22)


### Bug Fixes

* bin script ([e5b2e98](https://gitlab.com/depixy/image/commit/e5b2e984854dad7ce8d975edad5ce728967cb98d))

# [1.1.0](https://gitlab.com/depixy/image/compare/v1.0.2...v1.1.0) (2020-07-22)


### Features

* move configuration to external ([4845fa6](https://gitlab.com/depixy/image/commit/4845fa69125f24a36ca854214de3f449e3e97eee))

## [1.0.2](https://gitlab.com/depixy/image/compare/v1.0.1...v1.0.2) (2020-07-22)


### Bug Fixes

* add node to bin ([d94f352](https://gitlab.com/depixy/image/commit/d94f3529cfbd687ec8c22d22793c08d9a9082a51))

## [1.0.1](https://gitlab.com/depixy/image/compare/v1.0.0...v1.0.1) (2020-07-21)


### Bug Fixes

* publishConfig access to public ([f032d36](https://gitlab.com/depixy/image/commit/f032d3646d7ac74e87967f13ffa160b3bedb91df))

# 1.0.0 (2020-07-21)


### Bug Fixes

* add semantic-release ([3673e77](https://gitlab.com/depixy/image/commit/3673e77891bb5bd33de633accccf278682f9ae41))


### Features

* initial commit ([245d2db](https://gitlab.com/depixy/image/commit/245d2db31bff3b038d1da3ef43a68fe67bd3b122))
