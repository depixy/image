# @depixy/image

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Image proxy for Depixy.

## Installation

```
npm i @depixy/image
```

## Getting Started

### Start Application

```
depixy-image
```

or

```
npm run start
```

if you are in development. Note that you need to build the TypeScript first.

### Configuration

Environment variable `DEPIXY_CONFIG` need to be defined and point to a JavaScript file that export configuration.
See `config.example.js` for example.

The configuration is the following

```typescript
export interface Configuration {
  port: number;
  s3: {
    endPoint: string;
    useSSL: boolean;
    accessKey: string;
    secretKey: string;
    bucket: string;
    port: number;
  };
}
```

[license]: https://gitlab.com/depixy/image/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/depixy/image/pipelines
[pipelines_badge]: https://gitlab.com/depixy/image/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/depixy/image/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/@depixy/image
[npm_badge]: https://img.shields.io/npm/v/@depixy/image/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
