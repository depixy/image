import Koa, { Middleware } from "koa";
import logMiddleware from "@depixy/middleware-log";
import storageMiddleware from "@depixy/middleware-storage";
import S3Storage from "@depixy/storage-s3";

import { router } from "./router";
import { loadConfig } from "./config";

const useMiddleware = async (
  app: Koa,
  promises: Promise<Middleware>[]
): Promise<void> => {
  const middlewares = await Promise.all(promises);
  middlewares.forEach(app.use);
};

export const start = async (): Promise<void> => {
  const app = new Koa();
  const config = await loadConfig();
  await useMiddleware(app, [
    logMiddleware(),
    storageMiddleware({
      storage: new S3Storage(config.s3)
    })
  ]);
  app.use(router.routes()).use(router.allowedMethods());
  app.listen(config.port);
};
