import { getStr } from "@depixy/env";

const configPath = getStr("DEPIXY_CONFIG");

export const loadConfig = (): Promise<Configuration> => import(configPath);

export interface Configuration {
  port: number;
  s3: {
    endPoint: string;
    useSSL: boolean;
    accessKey: string;
    secretKey: string;
    bucket: string;
    port: number;
  };
}
