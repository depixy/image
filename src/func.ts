import _ from "lodash";

export const getEnvStringOrUndefined = (name: string): string | undefined =>
  process.env[name];

export const getEnvString = (name: string): string => {
  const env = getEnvStringOrUndefined(name);

  if (!env) {
    throw new Error(`${name} is undefined."`);
  }

  return env;
};

export const getEnvBoolean = (name: string, defaultValue: boolean): boolean => {
  const env = getEnvStringOrUndefined(name);
  return env?.toLowerCase() === (!defaultValue)?.toString()
    ? !defaultValue
    : defaultValue;
};

export const getEnvInt = (name: string): number => {
  const env = getEnvString(name);
  const value = parseInt(env);
  if (!_.isInteger(value)) {
    throw new Error(`${name} is not an integer."`);
  }
  return value;
};

export const getEnvIntOrUndefined = (
  name: string,
  defaultValue?: number
): number | undefined => {
  const env = getEnvStringOrUndefined(name);
  if (!env) {
    return undefined;
  }
  const value = parseInt(env);
  return _.isInteger(value) ? value : defaultValue;
};
