import sharp, { FitEnum, Sharp } from "sharp";
import _ from "lodash";

const getInt = (
  option: Record<string, string>,
  key: string
): number | undefined => (key in option ? _.toInteger(option[key]) : undefined);

const getBoolean = (
  option: Record<string, string>,
  key: string
): boolean | undefined =>
  key in option ? option[key]?.toLowerCase() === "true" : undefined;

export const transformImage = (option: Record<string, string>): Sharp => {
  let transformer = sharp();
  const resize = _.some(Object.keys(option), key => key.startsWith("resize-"));
  if (resize) {
    transformer = transformer.resize({
      width: getInt(option, "resize-width"),
      height: getInt(option, "resize-height"),
      fit: option["resize-fit"] as keyof FitEnum,
      withoutEnlargement: getBoolean(option, "resize-withoutEnlargement"),
      fastShrinkOnLoad: getBoolean(option, "resize-fastShrinkOnLoad")
    });
  }
  if ("format" in option) {
    transformer = transformer.toFormat(option["format"], {
      progressive: getBoolean(option, "format-progressive"),
      lossless: getBoolean(option, "format-lossless"),
      nearLossless: getBoolean(option, "format-nearLossless")
    });
  }
  return transformer;
};
