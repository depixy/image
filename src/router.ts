import Router from "@koa/router";
import koaBody from "koa-body";
import { createReadStream } from "fs";
import { getType } from "mime";
import _ from "lodash";

import { transformImage } from "./image";

export const router = new Router();

router.post("/", koaBody({ multipart: true }), async (ctx, next) => {
  const files = ctx.request.files || {};
  const promises = Object.entries(files).map(([key, file]) => {
    const rs = createReadStream(file.path);
    const metadata = file.type ? { "content-type": file.type } : undefined;
    return ctx.storage.save(key, rs, metadata);
  });
  ctx.status = 200;
  await Promise.all(promises);
  await next();
});

router.get("/", async (ctx, next) => {
  const { key, ...option } = ctx.query;
  if (key) {
    try {
      const [body, metadata] = await Promise.all([
        ctx.storage.load(key),
        ctx.storage.metadata(key)
      ]);
      if ("format" in option) {
        const mimeType = getType(option["format"]);
        if (mimeType) {
          ctx.set("Content-Type", mimeType);
        }
      } else if ("content-type" in metadata) {
        ctx.set("Content-Type", metadata["content-type"]);
      }
      if ("etag" in metadata) {
        ctx.set("ETag", metadata["etag"]);
      }
      if ("last-modified" in metadata) {
        ctx.set("Last-Modified", metadata["last-modified"]);
      }
      ctx.set("Cache-Control", "public, max-age=2592000");
      ctx.body = body.pipe(transformImage(option));
    } catch (e) {
      ctx.status = 400;
      ctx.body = e.message;
    }
  }
  await next();
});

router.delete("/", async (ctx, next) => {
  const { key, ...option } = ctx.query;
  if (key) {
    const keys = _.isArrayLike(key) ? key : [key];
    try {
      await ctx.storage.deleteMany(keys);
      ctx.status = 200;
    } catch (e) {
      ctx.status = 400;
      ctx.body = e.message;
    }
  }
  await next();
});
